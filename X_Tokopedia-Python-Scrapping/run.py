import requests
#import json
import csv

mykey=input("Masukkan Kunci Pencarian :")
write = csv.writer(open("hasil/{}".format(mykey),"w",newline=""))
header=["no","id","name","price","rating","review","shop","official","platform"]
write.writerow(header)

url = "https://gql.tokopedia.com/graphql/SearchProductQueryV4"
param = {"operationName":"SearchProductQueryV4","variables":{"params":"device=desktop&navsource=home&ob=23&page=1&q="+mykey+"&related=true&rows=60&safe_search=false&scheme=https&shipping=&source=search&srp_component_id=02.01.00.00&st=product&start=0&topads_bucket=true&unique_id=1bcca93e3e8b6dda4560501d403a2b10&user_addressId=62403013&user_cityId=151&user_districtId=1737&user_id=147077&user_lat=-6.463645913732336&user_long=107.04029649496078&user_postCode=16830&user_warehouseId=0&variants="},"query":"query SearchProductQueryV4($params: String!) {\n  ace_search_product_v4(params: $params) {\n    header {\n      totalData\n      totalDataText\n      processTime\n      responseCode\n      errorMessage\n      additionalParams\n      keywordProcess\n      componentId\n      __typename\n    }\n    data {\n      backendFilters\n      isQuerySafe\n      ticker {\n        text\n        query\n        typeId\n        componentId\n        trackingOption\n        __typename\n      }\n      redirection {\n        redirectUrl\n        departmentId\n        __typename\n      }\n      related {\n        position\n        trackingOption\n        relatedKeyword\n        otherRelated {\n          keyword\n          url\n          product {\n            id\n            name\n            price\n            imageUrl\n            rating\n            countReview\n            url\n            priceStr\n            wishlist\n            shop {\n              city\n              isOfficial\n              isPowerBadge\n              __typename\n            }\n            ads {\n              adsId: id\n              productClickUrl\n              productWishlistUrl\n              shopClickUrl\n              productViewUrl\n              __typename\n            }\n            badges {\n              title\n              imageUrl\n              show\n              __typename\n            }\n            ratingAverage\n            labelGroups {\n              position\n              type\n              title\n              url\n              __typename\n            }\n            componentId\n            __typename\n          }\n          componentId\n          __typename\n        }\n        __typename\n      }\n      suggestion {\n        currentKeyword\n        suggestion\n        suggestionCount\n        instead\n        insteadCount\n        query\n        text\n        componentId\n        trackingOption\n        __typename\n      }\n      products {\n        id\n        name\n        ads {\n          adsId: id\n          productClickUrl\n          productWishlistUrl\n          productViewUrl\n          __typename\n        }\n        badges {\n          title\n          imageUrl\n          show\n          __typename\n        }\n        category: departmentId\n        categoryBreadcrumb\n        categoryId\n        categoryName\n        countReview\n        customVideoURL\n        discountPercentage\n        gaKey\n        imageUrl\n        labelGroups {\n          position\n          title\n          type\n          url\n          __typename\n        }\n        originalPrice\n        price\n        priceRange\n        rating\n        ratingAverage\n        shop {\n          shopId: id\n          name\n          url\n          city\n          isOfficial\n          isPowerBadge\n          __typename\n        }\n        url\n        wishlist\n        sourceEngine: source_engine\n        __typename\n      }\n      violation {\n        headerText\n        descriptionText\n        imageURL\n        ctaURL\n        ctaApplink\n        buttonText\n        buttonType\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n}\n"}

#https://jsonformatter.curiousconcept.com/#
#print(r.status_code)
 
r = requests.post(url,json=param)
r1 = r.json()
#print(r1)
r2 = r1["data"]
#print(r2)
r3 = r2["ace_search_product_v4"]
#print(r3)
r4 = r3["data"]
#print(r4)
r5 = r4["products"]
#print(r5)

n=0
for p in r5:
    n=n+1
    s = p["shop"]
    line = [n,p["id"],p["name"],p["price"],p["rating"],p["countReview"],s["name"],s["isOfficial"],"Tokopedia"]
    write = csv.writer(open("hasil/{}".format(mykey),"a",newline=""))
    write.writerow(line)

