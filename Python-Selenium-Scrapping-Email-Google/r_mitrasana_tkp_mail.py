#pip install -U selenium
#pip install beautifulsoup4
#pip install undetected-chromedriver
#https://codebeautify.org/htmlviewer
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.action_chains import ActionChains
from bs4 import BeautifulSoup
import undetected_chromedriver.v2 as uc
import time
import psycopg2
from datetime import datetime
from selenium.webdriver.chrome.options import Options

_username = 'moshub.mitrasana@gmail.com'
_pass = "Moshub123!"
_partner = 'Mitrasana'
_platform = 'Tokopedia'

#options = uc.ChromeOptions()
#options.headless=True
#options.add_argument('--headless')
#driver = uc.Chrome(options=options)

class My_Chrome(uc.Chrome):
    def __del__(self):
        pass

print("\033[1;32m Start Scrapping Tokopedia \033[1;37m")
browser = My_Chrome(use_subprocess=True)
browser.delete_all_cookies()

browser.get('https://www.tokopedia.com/login?ld=https://seller.tokopedia.com/')

timeout = 10
try:
    element_present = EC.presence_of_element_located((By.XPATH,"/html/body/div[1]/div/div[2]/section/div[4]/button"))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m 001-Success waiting for google button to load \033[1;37m")
except TimeoutException:
    print("\033[1;32m 001-Timed out waiting for google button to load \033[1;37m")
    #browser.close()
    
try:
    e = browser.find_element(By.XPATH,"/html/body/div[1]/div/div[2]/section/div[4]/button")
    e.click()
    print("\033[1;32m 001-Click Success \033[1;37m")
except:
    print("\033[1;32m 001-Click Failed \033[1;37m")

try:
    window_before = browser.window_handles[0]
    window_after = browser.window_handles[1]
    browser.switch_to.window(window_after)
    browser.refresh()
    print("\033[1;32m 001-A-Switch Window After Success \033[1;37m")
except:
    print("\033[1;32m 001-A-Switch Window After Failed \033[1;37m")

try:
    element_present = EC.presence_of_element_located((By.CLASS_NAME,"whsOnd"))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m 002-Success waiting for google user name to load \033[1;37m")
except TimeoutException:
    print("\033[1;32m 002-Timed out waiting for google user name to load \033[1;37m")
    #browser.close()
    
try:
    e = browser.find_element(By.CLASS_NAME,"whsOnd")
    e.send_keys(_username)
    print("\033[1;32m 002-Send Keys Success \033[1;37m")
except:
    print("\033[1;32m 002-Send Keys Failed \033[1;37m")

try:
    element_present = EC.presence_of_element_located((By.XPATH,"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[2]/div/div[1]/div/div/button"))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m 003-Success waiting for Berikutnya button to load \033[1;37m")
except TimeoutException:
    print("\033[1;32m 003-Timed out waiting for Berikutnya button to load \033[1;37m")
    #browser.close()
    
try:
    e = browser.find_element(By.XPATH,"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[2]/div/div[1]/div/div/button")
    e.click()
    print("\033[1;32m 003-Click 1 Success \033[1;37m")
except:
    try:
        e = browser.find_element(By.XPATH,"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[2]/div/div[1]/div/div/button")
        e.click()
        print("\033[1;32m 003-Click 2 Success \033[1;37m")
    except:
        print("\033[1;32m 003-Click Failed \033[1;37m")

time.sleep(10)

try:
    element_present = EC.presence_of_element_located((By.XPATH,"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div[1]/div[3]/div/div[1]/div/div/div[1]/div/input"))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m 004-Success waiting for Tampilkan Sandi to load \033[1;37m")
except TimeoutException:
    print("\033[1;32m 004-Timed out waiting for Tampilkan Sandi to load \033[1;37m")
    #browser.close()
    
try:
    element_present = EC.presence_of_element_located((By.XPATH,"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div[1]/div[1]/div/div/div/div/div[1]/div/div[1]/input"))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m 005-Success waiting for google password to load \033[1;37m")
except TimeoutException:
    print("\033[1;32m 005-Timed out waiting for google password to load \033[1;37m")
    #browser.close()
    
try:
    e = browser.find_element(By.XPATH,"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div[1]/div[1]/div/div/div/div/div[1]/div/div[1]/input")
    e.send_keys(_pass)
    print("\033[1;32m 005-Send Keys 1 Success \033[1;37m")
except:
    try:
        e = browser.find_element(By.XPATH,"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div[1]/div[1]/div/div/div/div/div[1]/div/div[1]/input")
        e.send_keys(_pass)
        print("\033[1;32m 005-Send Keys 2 Success \033[1;37m")
    except:
        print("\033[1;32m 005-Send Keys Failed \033[1;37m")

try:
    element_present = EC.presence_of_element_located((By.XPATH,"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[2]/div/div[1]/div/div/button"))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m 006-Success waiting for Berikutnya button to load \033[1;37m")
except TimeoutException:
    print("\033[1;32m 006-Timed out waiting for Berikutnya button to load \033[1;37m")
    #browser.close()
    
try:
    e = browser.find_element(By.XPATH,"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[2]/div/div[1]/div/div/button")
    e.click()
    print("\033[1;32m 006-Click 1 Success \033[1;37m")
except:
    try:
        e = browser.find_element(By.XPATH,"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[2]/div/div[1]/div/div/button")
        e.click()
        print("\033[1;32m 006-Click 2 Success \033[1;37m")
    except:
        print("\033[1;32m 006-Click Failed \033[1;37m")

time.sleep(2)

try:
    browser.switch_to.window(window_before)
    print("\033[1;32m 007-Switch to window before Success \033[1;37m")
except:
    print("\033[1;32m 007-Switch to window before Failed \033[1;37m")

try:
    element_present = EC.presence_of_element_located((By.CLASS_NAME,"unf-card"))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m 008-Success waiting for email button to load \033[1;37m")
except TimeoutException:
    print("\033[1;32m 008-Timed out waiting for email button to load \033[1;37m")
    #browser.close()
    
try:
    e = browser.find_element(By.CLASS_NAME,"unf-card")
    e.click()
    print("\033[1;32m 008-Click 1 Success \033[1;37m")
except:
    try:
        e = browser.find_element(By.CLASS_NAME,"unf-card")
        e.click()
        print("\033[1;32m 008-Click 2 Success \033[1;37m")
    except:
        print("\033[1;32m 008-Click Failed \033[1;37m")
#Catatan kalau sudah 5 kali minta token via email, harus menunggu 7 menit

time.sleep(30)

#remark dari sini
try:
    browser2 = My_Chrome(use_subprocess=True)
    #browser2.quit()
    browser2.get('https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin')
    print("\033[1;32m 009-Open New Browser Success \033[1;37m")
except:
    print("\033[1;32m 009-Open New Browser Failed \033[1;37m")

try:
    element_present = EC.presence_of_element_located((By.CLASS_NAME,"whsOnd"))
    WebDriverWait(browser2, timeout).until(element_present)
    print("\033[1;32m 010-Success waiting for google user name to load \033[1;37m")
except TimeoutException:
    print("\033[1;32m 010-Timed out waiting for google user name to load \033[1;37m")
    #browser2.close()
    
try:
    e = browser2.find_element(By.CLASS_NAME,"whsOnd")
    e.send_keys(_username)
    print("\033[1;32m 010-Send Keys success \033[1;37m")
except:
    print("\033[1;32m 010-Send Keys failed \033[1;37m")

try:
    element_present = EC.presence_of_element_located((By.XPATH,"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[2]/div/div[1]/div/div/button"))
    WebDriverWait(browser2, timeout).until(element_present)
    print("\033[1;32m 011-Success waiting for Berikutnya button to load \033[1;37m")
except TimeoutException:
    print("\033[1;32m 011-Timed out waiting for Berikutnya button to load \033[1;37m")
    #browser2.close()
    
try:
    e = browser2.find_element(By.XPATH,"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[2]/div/div[1]/div/div/button")
    e.click()
    print("\033[1;32m 011-Click 1 Success \033[1;37m")
except:
    try:
        e = browser2.find_element(By.XPATH,"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[2]/div/div[1]/div/div/button")
        e.click()
        print("\033[1;32m 011-Click 2 Success \033[1;37m")
    except:
        print("\033[1;32m 011-Click Failed \033[1;37m")

time.sleep(20)

try:
    element_present = EC.presence_of_element_located((By.XPATH,"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div[1]/div[3]/div/div[1]/div/div/div[1]/div/input"))
    WebDriverWait(browser2, timeout).until(element_present)
    print("\033[1;32m 012-Success waiting for Tampilkan Sandi to load \033[1;37m")
except TimeoutException:
    print("\033[1;32m 012-Timed out waiting for Tampilkan Sandi to load \033[1;37m")
    #browser2.close()
    
try:
    element_present = EC.presence_of_element_located((By.XPATH,"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div[1]/div[1]/div/div/div/div/div[1]/div/div[1]/input"))
    WebDriverWait(browser2, timeout).until(element_present)
    print("\033[1;32m 013-Success waiting for google password to load \033[1;37m")
except TimeoutException:
    print("\033[1;32m 013-Timed out waiting for google password to load \033[1;37m")
    #browser.close()
    
try:
    e = browser2.find_element(By.XPATH,"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div[1]/div[1]/div/div/div/div/div[1]/div/div[1]/input")
    e.send_keys(_pass)
    print("\033[1;32m 013-Send Keys 1 Success \033[1;37m")
except:
    try:
       e = browser2.find_element(By.CLASS_NAME,"whsOnd")
       e.send_keys(_pass)
       print("\033[1;32m 013-Send Keys 2 Success \033[1;37m")
    except:
       print("\033[1;32m 013-Send Keys Failed \033[1;37m")

try:
    element_present = EC.presence_of_element_located((By.XPATH,"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[2]/div/div[1]/div/div/button"))
    WebDriverWait(browser2, timeout).until(element_present)
    print("\033[1;32m 014-Success waiting for Berikutnya button to load \033[1;37m")
except TimeoutException:
    print("\033[1;32m 014-Timed out waiting for Berikutnya button to load \033[1;37m")
    #browser.close()
    
try:
    e = browser2.find_element(By.XPATH,"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[2]/div/div[1]/div/div/button")
    e.click()
    print("\033[1;32m 014-Click 1 Success \033[1;37m")
except:
    try:
        e = browser2.find_element(By.XPATH,"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[2]/div/div[1]/div/div/button")
        e.click()
        print("\033[1;32m 014-Click 2 Success \033[1;37m")
    except:
        print("\033[1;32m 014-Click Failed \033[1;37m")

try:
    element_present = EC.presence_of_element_located((By.XPATH,"/html/body/div[7]/div[3]/div/div[1]/div[4]/div[1]/div/div[3]/div/div/div[2]/div"))
    WebDriverWait(browser2, timeout).until(element_present)
    print("\033[1;32m 014-Success waiting for X button to load \033[1;37m")
except TimeoutException:
    print("\033[1;32m 014-Timed out waiting for X button to load \033[1;37m")
    #browser.close()
    
try:
    e = browser2.find_element(By.XPATH,"/html/body/div[7]/div[3]/div/div[1]/div[4]/div[1]/div/div[3]/div/div/div[2]/div")
    e.click()
    print("\033[1;32m 014-Click X 1 Success \033[1;37m")
except:
    try:
        e = browser2.find_element(By.XPATH,"/html/body/div[7]/div[3]/div/div[1]/div[4]/div[1]/div/div[3]/div/div/div[2]/div")
        e.click()
        print("\033[1;32m 014-Click X 2 Success \033[1;37m")
    except:
        print("\033[1;32m 014-Click X Failed \033[1;37m")

try:
    element_present = EC.presence_of_element_located((By.XPATH,"/html/body/div[7]/div[3]/div/div[1]/div[3]/header/div[2]/div[2]/div[2]/form/div/table/tbody/tr/td/table/tbody/tr/td/div/input[1]"))
    WebDriverWait(browser2, timeout).until(element_present)
    print("\033[1;32m 015-Success waiting for Email Searching to load \033[1;37m")
except TimeoutException:
    print("\033[1;32m 015-Timed out waiting for Email Searching to load \033[1;37m")
    #browser.close()
    
try:
    e = browser2.find_element(By.XPATH,"/html/body/div[7]/div[3]/div/div[1]/div[3]/header/div[2]/div[2]/div[2]/form/div/table/tbody/tr/td/table/tbody/tr/td/div/input[1]")
    e.send_keys("Verifikasi Pertanyaan Keamanan")
    e = browser2.find_element(By.XPATH,"/html/body/div[7]/div[3]/div/div[1]/div[3]/header/div[2]/div[2]/div[2]/form/button[4]")
    e.click()
    print("\033[1;32m 016-Filtering Success \033[1;37m")
except:
    print("\033[1;32m 016-Filtering Failed \033[1;37m")

time.sleep(20)

try:
    element_present = EC.presence_of_element_located((By.CLASS_NAME,"zA"))
    WebDriverWait(browser2, timeout).until(element_present)
    print("\033[1;32m 017-Success waiting for Content to load \033[1;37m")
except TimeoutException:
    print("\033[1;32m 017-Timed out waiting for Content to load \033[1;37m")
    #browser.close()

e = browser2.find_element(By.XPATH,'//span[contains(text(), "verifikasi pertanyaan keamanan.")][1]')
f = e.get_attribute('innerHTML')
#print(f)
s = 'pertanyaan keamanan.'
z = f.index(s)
mytoken = f[(z+21):(z+27)]

#browser2.close()

time.sleep(2)
 
try:
    element_present = EC.presence_of_element_located((By.XPATH,"/html/body/div[1]/div/div[2]/section/div/div/input"))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m 009-Success waiting for token input to load \033[1;37m")
except TimeoutException:
    print("\033[1;32m 009-Timed out waiting for token input to load \033[1;37m")
    browser.close()
    
try:
    e = browser.find_element(By.XPATH,"/html/body/div[1]/div/div[2]/section/div/div/input")
    e.send_keys(mytoken)
    print("\033[1;32m 009-Send Keys 1 Success \033[1;37m")
except:
    try:
        e = browser.find_element(By.XPATH,"/html/body/div[1]/div/div[2]/section/div/div/input")
        e.send_keys(mytoken)
        print("\033[1;32m 009-Send Keys 2 Success \033[1;37m")
    except:
        print("\033[1;32m 009-Send Keys Failed \033[1;37m")


timeout = 1000    
try:
    element_present = EC.presence_of_element_located((By.CLASS_NAME, 'css-evb2vf-unf-btn'))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m Success waiting for main page to load \033[1;37m")
except TimeoutException:
    print("\033[1;32m Timed out waiting for main page to load \033[1;37m")
    #browser.close()

timeout = 10
time.sleep(10)

try:
    element_present = EC.presence_of_element_located((By.XPATH, '/html/body/div[2]/div[2]/button'))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m Success waiting for Tambah Nomor HP \033[1;37m")
except TimeoutException:
    print("\033[1;32m Timed out waiting for Tambah Nomor HP \033[1;37m")
    #browser.close()
try:
    e = browser.find_element(By.XPATH,"/html/body/div[2]/div[2]/button")
    e.click()
except:
    time.sleep(1)

time.sleep(10)

try:
    element_present = EC.presence_of_element_located((By.XPATH, '/html/body/div[4]/div[2]/div[1]/div[2]/div/label/span'))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m Success waiting for click-1 \033[1;37m")
except TimeoutException:
    print("\033[1;32m Timed out waiting for click-1 \033[1;37m")
    #browser.close()
try:
    e = browser.find_element(By.XPATH,"/html/body/div[4]/div[2]/div[1]/div[2]/div/label/span")
    #browser.execute_script("arguments[0].click();", e)
    e.click()
except:
    try:
        e = browser.find_element(By.XPATH,"/html/body/div[4]/div[2]/div[1]/div[2]/div/label/input")
        e.click()
    except:
        try:
            e = browser.find_element(By.XPATH,"/html/body/div[4]/div[2]/div[1]/div[2]/div/label")
            e.click()
        except:
            print("\033[1;32m Failed click-1 \033[1;37m")

try:
    element_present = EC.presence_of_element_located((By.XPATH, '/html/body/div[4]/div[2]/div[2]/button'))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m Success waiting for click-1 \033[1;37m")
except TimeoutException:
    print("\033[1;32m Timed out waiting for click-1 \033[1;37m")
    #browser.close()
try:
    e = browser.find_element(By.XPATH,"/html/body/div[4]/div[2]/div[2]/button")
    e.click()
except:
    try:
        e = browser.find_element(By.XPATH,"/html/body/div[4]/div[2]/div[2]/button")
        e.click()
    except:
        print("\033[1;32m Failed click-1 \033[1;37m")

time.sleep(10)
try:
    e = WebDriverWait(browser, timeout).until(EC.presence_of_element_located((By.XPATH, 
        '/html/body/div[1]/div/div/div[1]/header/div[2]/div/div[2]/div/div[1]/input')))
    action = ActionChains(browser)
    action.move_to_element(e).move_by_offset(250, 0).click().perform()
    print("\033[1;32m Success waiting for click block modal \033[1;37m")
except TimeoutException:
    print("\033[1;32m Timed out waiting for click block modal \033[1;37m")
    #browser.close()

try:
    element_present = EC.presence_of_element_located((By.XPATH, '/html/body/div[1]/div/div[2]/div/div/div[2]/div[3]/button[1]'))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m Success waiting for click-1 \033[1;37m")
except TimeoutException:
    print("\033[1;32m Timed out waiting for click-1 \033[1;37m")
    #browser.close()
try:
    e = browser.find_element(By.XPATH,"/html/body/div[1]/div/div[2]/div/div/div[2]/div[3]/button[1]")
    e.click()
except:
    try:
        e = browser.find_element(By.XPATH,"/html/body/div[1]/div/div[2]/div/div/div[2]/div[3]/button[1]")
        e.click()
    except:
        print("\033[1;32m Failed click-1 \033[1;37m")

try:
    element_present = EC.presence_of_element_located((By.PARTIAL_LINK_TEXT,"Wawasan Produk"))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m Success waiting for wawasan product link \033[1;37m")
except TimeoutException:
    print("\033[1;32m Timed out waiting for wawasan product link \033[1;37m")
    #browser.close()
e = browser.find_element(By.PARTIAL_LINK_TEXT,"Wawasan Produk")
e.click()

try:
    element_present = EC.presence_of_element_located((By.XPATH,"/html/body/div[5]/div[7]/section/div/div/div[2]/div"))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m Success waiting for pilihan kalender \033[1;37m")
except TimeoutException:
    print("\033[1;32m Timed out waiting for pilihan kalender \033[1;37m")
    #browser.close()
try:
    e = browser.find_element(By.XPATH,"/html/body/div[5]/div[7]/section/div/div/div[2]/div")
    e.click()
except:
    print("\033[1;32m Failed kalender \033[1;37m")

try:
    element_present = EC.presence_of_element_located((By.XPATH,"/html/body/div[6]/div[7]/section/div/div/div[2]/div[1]"))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m Success waiting for Lewati \033[1;37m")
except TimeoutException:
    print("\033[1;32m Timed out waiting for Lewati \033[1;37m")
    #browser.close()
try:    
    e = browser.find_element(By.XPATH,"/html/body/div[6]/div[7]/section/div/div/div[2]/div[1]")
    e.click()
except:
    print("\033[1;32m Failed Lewati \033[1;37m")

time.sleep(10)

browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")

kondisi=False
timeout=20

connection = psycopg2.connect(user="moshub_konso",
                                  password="konsomoshub",
                                  host="13.251.7.50",
                                  port="39726",
                                  database="dbskonso")
cursor = connection.cursor()

postgres_delete_query = """ 
                delete from t_wawasanprod where username=%s and platform=%s and partner=%s
                """
where_clause = (_username,_platform,_partner)
cursor.execute(postgres_delete_query, where_clause)
connection.commit()

while not kondisi:
    try:
        element_present = EC.presence_of_element_located((By.CSS_SELECTOR,'[aria-label="Halaman berikutnya"]'))
        WebDriverWait(browser, timeout).until(element_present)
        print("\033[1;32m Success waiting for next page \033[1;37m")
    except TimeoutException:
        print("\033[1;32m Timed out waiting for next page \033[1;37m")
        #browser.close()
    e = browser.find_element(By.CSS_SELECTOR,'[aria-label="Halaman berikutnya"]')
    prop = e.get_property('disabled')
    kondisi=prop
    #print("")
    p = browser.find_element(By.CLASS_NAME,"css-1ptv35")
    f = p.get_attribute('innerHTML')
    #print(f)        
    soup = BeautifulSoup(f,features="html.parser")
    table = soup.find("table")
    output_rows = []
    for table_row in table.findAll('tr'):
        columns = table_row.findAll('td')
        output_row = []
        for column in columns:
            output_row.append(column.text)
            #print("test ",column.text)
        #output_rows.append(output_row)
        insdata = output_row
        #print(insdata)
        if (insdata[1]!=""):
            _text=insdata[1]
            _cari = 'SKU:'
            _cari2 = ' - Kategori:'
            z = _text.index(_cari)
            z2 = _text.index(_cari2)
            _nmproduk = _text[0:(z)]
            _sku = _text[(z+5):(z2)]
            _kategori = _text[(z2+13):(len(_text))]
            #print('nmproduk   : ',_nmproduk)
            #print('sku        : ',_sku)
            #print('kategori   : ',_kategori)
            now = datetime.now()
            dt_string = now.strftime("%m/%d/%Y %H:%M:%S")
            #print("tglproses  : ", dt_string)
            _text=insdata[2]
            _pendapatan=_text.replace("Rp","",1)
            _pendapatan=_pendapatan.replace(".","",10)
            #print('pendapatan : ',_pendapatan)
            _terjual=insdata[3].replace(".","",10)
            #print('terjual    : ',_terjual)
            _dilihat=insdata[4].replace(".","",10)
            #print('dilihat    : ',_dilihat)
            _konversi=insdata[7].replace("%","",1)
            _konversi=_konversi.replace(",",".",1)
            #print('konversi    : ',_konversi)
            postgres_insert_query = """ 
                insert into t_wawasanprod (
                    username,platform,partner,prosesdate,sku,
                    nmproduk,pendapatan,terjual,dilihat,konversi,kategori
                )
                values 
                (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"""
            record_to_insert = (_username,_platform,_partner,dt_string,_sku,
                _nmproduk,_pendapatan,_terjual,_dilihat,_konversi,_kategori)
            cursor.execute(postgres_insert_query, record_to_insert)

    connection.commit()
    #print(output_rows)
    if not kondisi:
        e.click()

cursor.close()
connection.close()

time.sleep(20)
print("===== Success =========")

browser.close()
browser2.close()
