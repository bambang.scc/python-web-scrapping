#pip install -U selenium
#pip install beautifulsoup4
#pip install undetected-chromedriver
#pip install psycopg2
#https://codebeautify.org/htmlviewer
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.action_chains import ActionChains
from bs4 import BeautifulSoup
from gmail2 import get_email
import undetected_chromedriver.v2 as uc
import time
import psycopg2

class My_Chrome(uc.Chrome):
    def __del__(self):
        pass
print("\033[1;32m Start Scrapping Tokopedia \033[1;37m")
browser = My_Chrome(use_subprocess=True)
browser.delete_all_cookies()

myemail = "moshub.mitrasana@gmail.com"
mypass = "Moshub123!"

browser.get('https://www.tokopedia.com/login?ld=https://seller.tokopedia.com/')

timeout = 10
try:
    element_present = EC.presence_of_element_located((By.XPATH,"/html/body/div[1]/div/div[2]/section/div[4]/button"))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m 001-Success waiting for google button to load \033[1;37m")
except TimeoutException:
    print("\033[1;32m 001-Timed out waiting for google button to load \033[1;37m")
    browser.close()
    
try:
    e = browser.find_element(By.XPATH,"/html/body/div[1]/div/div[2]/section/div[4]/button")
    e.click()
    print("\033[1;32m 001-Click Success \033[1;37m")
except:
    print("\033[1;32m 001-Click Failed \033[1;37m")

try:
    window_before = browser.window_handles[0]
    window_after = browser.window_handles[1]
    browser.switch_to.window(window_after)
    browser.refresh()
    print("\033[1;32m 001-A-Switch Window After Success \033[1;37m")
except:
    print("\033[1;32m 001-A-Switch Window After Failed \033[1;37m")

try:
    element_present = EC.presence_of_element_located((By.CLASS_NAME,"whsOnd"))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m 002-Success waiting for google user name to load \033[1;37m")
except TimeoutException:
    print("\033[1;32m 002-Timed out waiting for google user name to load \033[1;37m")
    browser.close()
    
try:
    e = browser.find_element(By.CLASS_NAME,"whsOnd")
    #e.send_keys("ecc.tokopedia@gmail.com")
    e.send_keys(myemail)
    #e.send_keys("bambang.scc@gmail.com")
    print("\033[1;32m 002-Send Keys Success \033[1;37m")
except:
    print("\033[1;32m 002-Send Keys Failed \033[1;37m")

try:
    element_present = EC.presence_of_element_located((By.XPATH,"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[2]/div/div[1]/div/div/button"))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m 003-Success waiting for Berikutnya button to load \033[1;37m")
except TimeoutException:
    print("\033[1;32m 003-Timed out waiting for Berikutnya button to load \033[1;37m")
    browser.close()
    
try:
    e = browser.find_element(By.XPATH,"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[2]/div/div[1]/div/div/button")
    e.click()
    print("\033[1;32m 003-Click 1 Success \033[1;37m")
except:
    try:
        e = browser.find_element(By.XPATH,"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[2]/div/div[1]/div/div/button")
        e.click()
        print("\033[1;32m 003-Click 2 Success \033[1;37m")
    except:
        print("\033[1;32m 003-Click Failed \033[1;37m")

time.sleep(10)

try:
    element_present = EC.presence_of_element_located((By.XPATH,"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div[1]/div[3]/div/div[1]/div/div/div[1]/div/input"))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m 004-Success waiting for Tampilkan Sandi to load \033[1;37m")
except TimeoutException:
    print("\033[1;32m 004-Timed out waiting for Tampilkan Sandi to load \033[1;37m")
    browser.close()
    
try:
    element_present = EC.presence_of_element_located((By.XPATH,"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div[1]/div[1]/div/div/div/div/div[1]/div/div[1]/input"))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m 005-Success waiting for google password to load \033[1;37m")
except TimeoutException:
    print("\033[1;32m 005-Timed out waiting for google password to load \033[1;37m")
    browser.close()
    
try:
    e = browser.find_element(By.XPATH,"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div[1]/div[1]/div/div/div/div/div[1]/div/div[1]/input")
    #e.send_keys("JbDonsD28!!")
    #e.send_keys("M")
    e.send_keys(mypass)
    print("\033[1;32m 005-Send Keys 1 Success \033[1;37m")
except:
    try:
        e = browser.find_element(By.XPATH,"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div[1]/div[1]/div/div/div/div/div[1]/div/div[1]/input")
        #e.send_keys("JbDonsD28!!")
        #e.send_keys("Meikarta71")
        e.send_keys(mypass)
        print("\033[1;32m 005-Send Keys 2 Success \033[1;37m")
    except:
        print("\033[1;32m 005-Send Keys Failed \033[1;37m")

try:
    element_present = EC.presence_of_element_located((By.XPATH,"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[2]/div/div[1]/div/div/button"))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m 006-Success waiting for Berikutnya button to load \033[1;37m")
except TimeoutException:
    print("\033[1;32m 006-Timed out waiting for Berikutnya button to load \033[1;37m")
    browser.close()
    
try:
    e = browser.find_element(By.XPATH,"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[2]/div/div[1]/div/div/button")
    e.click()
    print("\033[1;32m 006-Click 1 Success \033[1;37m")
except:
    try:
        e = browser.find_element(By.XPATH,"/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[2]/div/div[1]/div/div/button")
        e.click()
        print("\033[1;32m 006-Click 2 Success \033[1;37m")
    except:
        print("\033[1;32m 006-Click Failed \033[1;37m")

time.sleep(2)

try:
    browser.switch_to.window(window_before)
    print("\033[1;32m 007-Switch to window before Success \033[1;37m")
except:
    print("\033[1;32m 007-Switch to window before Failed \033[1;37m")

try:
    element_present = EC.presence_of_element_located((By.CLASS_NAME,"unf-card"))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m 008-Success waiting for email button to load \033[1;37m")
except TimeoutException:
    print("\033[1;32m 008-Timed out waiting for email button to load \033[1;37m")
    browser.close()
    
try:
    e = browser.find_element(By.CLASS_NAME,"unf-card")
    e.click()
    print("\033[1;32m 008-Click 1 Success \033[1;37m")
except:
    try:
        e = browser.find_element(By.CLASS_NAME,"unf-card")
        e.click()
        print("\033[1;32m 008-Click 2 Success \033[1;37m")
    except:
        print("\033[1;32m 008-Click Failed \033[1;37m")
#Catatan kalau sudah 5 kali minta token via email, harus menunggu 7 menit

time.sleep(2)

mytoken = get_email(myemail, mypass )        

try:
    element_present = EC.presence_of_element_located((By.XPATH,"/html/body/div[1]/div/div[2]/section/div/div/input"))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m 009-Success waiting for token input to load \033[1;37m")
except TimeoutException:
    print("\033[1;32m 009-Timed out waiting for token input to load \033[1;37m")
    browser.close()
    
try:
    e = browser.find_element(By.XPATH,"/html/body/div[1]/div/div[2]/section/div/div/input")
    e.send_keys(mytoken)
    print("\033[1;32m 009-Send Keys 1 Success \033[1;37m")
except:
    try:
        e = browser.find_element(By.XPATH,"/html/body/div[1]/div/div[2]/section/div/div/input")
        e.send_keys(mytoken)
        print("\033[1;32m 009-Send Keys 2 Success \033[1;37m")
    except:
        print("\033[1;32m 009-Send Keys Failed \033[1;37m")


timeout = 1000    
try:
    element_present = EC.presence_of_element_located((By.CLASS_NAME, 'css-evb2vf-unf-btn'))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m Success waiting for main page to load \033[1;37m")
except TimeoutException:
    print("\033[1;32m Timed out waiting for main page to load \033[1;37m")
    browser.close()

timeout = 10

try:
    element_present = EC.presence_of_element_located((By.XPATH, '/html/body/div[2]/div[2]/button'))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m Success waiting for Tambah Nomor HP \033[1;37m")
except TimeoutException:
    print("\033[1;32m Timed out waiting for Tambah Nomor HP \033[1;37m")
    #browser.close()
try:
    e = browser.find_element(By.XPATH,"/html/body/div[2]/div[2]/button")
    e.click()
except:
    time.sleep(1)


try:
    element_present = EC.presence_of_element_located((By.XPATH, '/html/body/div[4]/div[2]/div[1]/div[2]/div/label/span'))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m Success waiting for click-1 \033[1;37m")
except TimeoutException:
    print("\033[1;32m Timed out waiting for click-1 \033[1;37m")
    #browser.close()
try:
    e = browser.find_element(By.XPATH,"/html/body/div[4]/div[2]/div[1]/div[2]/div/label/span")
    #browser.execute_script("arguments[0].click();", e)
    e.click()
except:
    try:
        e = browser.find_element(By.XPATH,"/html/body/div[4]/div[2]/div[1]/div[2]/div/label/input")
        e.click()
    except:
        e = browser.find_element(By.XPATH,"/html/body/div[4]/div[2]/div[1]/div[2]/div/label")
        e.click()

try:
    element_present = EC.presence_of_element_located((By.XPATH, '/html/body/div[4]/div[2]/div[2]/button'))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m Success waiting for click-1 \033[1;37m")
except TimeoutException:
    print("\033[1;32m Timed out waiting for click-1 \033[1;37m")
    #browser.close()
try:
    e = browser.find_element(By.XPATH,"/html/body/div[4]/div[2]/div[2]/button")
    e.click()
except:
    e = browser.find_element(By.XPATH,"/html/body/div[4]/div[2]/div[2]/button")
    e.click()

time.sleep(10)
try:
    e = WebDriverWait(browser, timeout).until(EC.presence_of_element_located((By.XPATH, 
        '/html/body/div[1]/div/div/div[1]/header/div[2]/div/div[2]/div/div[1]/input')))
    action = ActionChains(browser)
    action.move_to_element(e).move_by_offset(250, 0).click().perform()
    print("\033[1;32m Success waiting for click block modal \033[1;37m")
except TimeoutException:
    print("\033[1;32m Timed out waiting for click block modal \033[1;37m")
    browser.close()

try:
    element_present = EC.presence_of_element_located((By.XPATH, '/html/body/div[1]/div/div[2]/div/div/div[2]/div[3]/button[1]'))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m Success waiting for click-1 \033[1;37m")
except TimeoutException:
    print("\033[1;32m Timed out waiting for click-1 \033[1;37m")
    browser.close()
try:
    e = browser.find_element(By.XPATH,"/html/body/div[1]/div/div[2]/div/div/div[2]/div[3]/button[1]")
    e.click()
except:
    e = browser.find_element(By.XPATH,"/html/body/div[1]/div/div[2]/div/div/div[2]/div[3]/button[1]")
    e.click()

try:
    element_present = EC.presence_of_element_located((By.PARTIAL_LINK_TEXT,"Wawasan Produk"))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m Success waiting for wawasan product link \033[1;37m")
except TimeoutException:
    print("\033[1;32m Timed out waiting for wawasan product link \033[1;37m")
    browser.close()
e = browser.find_element(By.PARTIAL_LINK_TEXT,"Wawasan Produk")
e.click()

try:
    element_present = EC.presence_of_element_located((By.XPATH,"/html/body/div[5]/div[7]/section/div/div/div[2]/div"))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m Success waiting for pilihan kalender \033[1;37m")
except TimeoutException:
    print("\033[1;32m Timed out waiting for pilihan kalender \033[1;37m")
    browser.close()
e = browser.find_element(By.XPATH,"/html/body/div[5]/div[7]/section/div/div/div[2]/div")
e.click()

try:
    element_present = EC.presence_of_element_located((By.XPATH,"/html/body/div[6]/div[7]/section/div/div/div[2]/div[1]"))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m Success waiting for Lewati \033[1;37m")
except TimeoutException:
    print("\033[1;32m Timed out waiting for Lewati \033[1;37m")
    browser.close()
e = browser.find_element(By.XPATH,"/html/body/div[6]/div[7]/section/div/div/div[2]/div[1]")
e.click()

time.sleep(10)

browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")

kondisi=False
timeout=20

connection = psycopg2.connect(user="moshub_konso",
                                  password="konsomoshub",
                                  host="13.251.7.50",
                                  port="39726",
                                  database="dbskonso")
cursor = connection.cursor()

while not kondisi:
    try:
        element_present = EC.presence_of_element_located((By.CSS_SELECTOR,'[aria-label="Halaman berikutnya"]'))
        WebDriverWait(browser, timeout).until(element_present)
        print("\033[1;32m Success waiting for next page \033[1;37m")
    except TimeoutException:
        print("\033[1;32m Timed out waiting for next page \033[1;37m")
        browser.close()
    e = browser.find_element(By.CSS_SELECTOR,'[aria-label="Halaman berikutnya"]')
    prop = e.get_property('disabled')
    kondisi=prop
    print("")
    p = browser.find_element(By.CLASS_NAME,"css-1ptv35")
    f = p.get_attribute('innerHTML')
    #print(f)        
    soup = BeautifulSoup(f,features="html.parser")
    table = soup.find("table")
    output_rows = []
    for table_row in table.findAll('tr'):
        columns = table_row.findAll('td')
        output_row = []
        for column in columns:
            output_row.append(column.text)
            #print("test ",column.text)
        #output_rows.append(output_row)
        insdata = output_row
        #print(insdata)
        if (insdata[1]!=""):
            _nmproduk=insdata[1]
            postgres_insert_query = """ 
                insert into t_wawasanprod (
                    username,platform,partner,prosesdate,sku,
                    nmproduk,pendapatan,terjual,dilihat,konversi
                )
                values 
                (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"""
            record_to_insert = ('','','','01-JAN-2022','',
                _nmproduk,0,0,0,0)
            cursor.execute(postgres_insert_query, record_to_insert)

    connection.commit()
    print(output_rows)
    if not kondisi:
        e.click()

cursor.close()
connection.close()

time.sleep(20)
print("===== Success =========")
#browser.close()
