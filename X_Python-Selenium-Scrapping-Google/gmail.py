import base64
import smtplib
import time
import imaplib
import email
import traceback
# -------------------------------------------------
#
# Utility to read email from Gmail Using Python
#
#Login into Gmail
#Go to Google Account
#Navigate to Security section
#Turn on access for Less secure app access
# ------------------------------------------------
ORG_EMAIL = "@gmail.com" 
FROM_EMAIL = "moshub.mitrasana" + ORG_EMAIL 
FROM_PWD = "Moshub123!" 
SMTP_SERVER = "imap.gmail.com" 
SMTP_PORT = 993

def read_email_from_gmail():
    try:
        mail = imaplib.IMAP4_SSL(SMTP_SERVER)
        mail.login(FROM_EMAIL,FROM_PWD)
        mail.select('inbox')

        #data = mail.search(None, 'ALL')
        #data = mail.search(None, '(FROM "anjali sinha" SUBJECT "test")' )
        data = mail.search(None, '(FROM "Tokopedia" SUBJECT "Verifikasi Pertanyaan Keamanan" SINCE 31-MAY-2022)' )
        mail_ids = data[1]
        id_list = mail_ids[0].split()   
        first_email_id = int(id_list[0])
        latest_email_id = int(id_list[-1])

        for i in range(latest_email_id,first_email_id, -1):
            data = mail.fetch(str(i), '(RFC822)' )
            for response_part in data:
                arr = response_part[0]
                if isinstance(arr, tuple):
                    #msg = email.message_from_string(str(arr[1],'utf-8'))
                    msg = email.message_from_string(str(arr[1],'utf-8'))
                    email_subject =  msg['subject']
                    email_from = msg['from']
                    email_text = msg['body']
                    print('From : ' + email_from + '\n')
                    print('Subject : ' + email_subject + '\n')
                    print(email_text)

    except Exception as e:
        traceback.print_exc() 
        print(str(e))

read_email_from_gmail()