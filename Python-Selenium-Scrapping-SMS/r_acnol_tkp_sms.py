#pip install -U selenium
#pip install beautifulsoup4
#pip install undetected-chromedriver
#https://codebeautify.org/htmlviewer
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.action_chains import ActionChains
from bs4 import BeautifulSoup
import undetected_chromedriver.v2 as uc
import time
import psycopg2
from datetime import datetime

_username = 'acnol.care.moshub@gmail.com'
_pass = "m3j1kuEnseval123"
_partner = 'Acnol'
_platform = 'Tokopedia'

class My_Chrome(uc.Chrome):
    def __del__(self):
        pass
print("\033[1;32m Start Scrapping Tokopedia \033[1;37m")
browser = My_Chrome(use_subprocess=True)
browser.delete_all_cookies()

browser.get('https://www.tokopedia.com/login?ld=https://seller.tokopedia.com/')

timeout = 10

try:
    element_present = EC.presence_of_element_located((By.XPATH,"/html/body/div[1]/div/div[2]/section/div[2]/form/div[1]/div[1]/div/input"))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m 001-Success waiting for user name to load \033[1;37m")
except TimeoutException:
    print("\033[1;32m 001-Timed out waiting for user name to load \033[1;37m")
    #browser.close()
    
try:
    e = browser.find_element(By.XPATH,"/html/body/div[1]/div/div[2]/section/div[2]/form/div[1]/div[1]/div/input")
    e.send_keys(_username)
    print("\033[1;32m 001-Send Keys Success \033[1;37m")
except:
    print("\033[1;32m 001-Send Keys Failed \033[1;37m")

try:
    element_present = EC.presence_of_element_located((By.XPATH,"/html/body/div[1]/div/div[2]/section/div[2]/form/button"))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m 002-Success waiting for selanjutnya button to load \033[1;37m")
except TimeoutException:
    print("\033[1;32m 002-Timed out waiting for selanjutnya button to load \033[1;37m")
    #browser.close()
    
try:
    e = browser.find_element(By.XPATH,"/html/body/div[1]/div/div[2]/section/div[2]/form/button")
    e.click()
    print("\033[1;32m 002-Click Success \033[1;37m")
except:
    print("\033[1;32m 002-Click Failed \033[1;37m")

time.sleep(10)

try:
    element_present = EC.presence_of_element_located((By.XPATH,"/html/body/div[1]/div/div[2]/section/div[2]/form/div[2]/div/div/input"))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m 003-Success waiting for password to load \033[1;37m")
except TimeoutException:
    print("\033[1;32m 003-Timed out waiting for password to load \033[1;37m")
    #browser.close()
    
try:
    e = browser.find_element(By.XPATH,"/html/body/div[1]/div/div[2]/section/div[2]/form/div[2]/div/div/input")
    e.send_keys(_pass)
    print("\033[1;32m 003-Send Keys Success \033[1;37m")
except:
    print("\033[1;32m 003-Send Keys Failed \033[1;37m")

try:
    element_present = EC.presence_of_element_located((By.XPATH,"/html/body/div[1]/div/div[2]/section/div[2]/form/button"))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m 004-Success waiting for Masuk button to load \033[1;37m")
except TimeoutException:
    print("\033[1;32m 004-Timed out waiting for Masuk button to load \033[1;37m")
    #browser.close()
    
try:
    e = browser.find_element(By.XPATH,"/html/body/div[1]/div/div[2]/section/div[2]/form/button")
    e.click()
    print("\033[1;32m 004-Click Success \033[1;37m")
except:
    print("\033[1;32m 004-Click Failed \033[1;37m")

try:
    element_present = EC.presence_of_element_located((By.CLASS_NAME,"unf-card"))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m 005-Success waiting for SMS button to load \033[1;37m")
except TimeoutException:
    print("\033[1;32m 005-Timed out waiting for SMS button to load \033[1;37m")
    #browser.close()
    
try:
    e = browser.find_element(By.CLASS_NAME,"unf-card")
    e.click()
    print("\033[1;32m 005-Click Success \033[1;37m")
except:
    print("\033[1;32m 005-Click Failed \033[1;37m")

time.sleep(50)

try:
    element_present = EC.presence_of_element_located((By.CLASS_NAME,"css-evb2vf-unf-btn"))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m 006-Success waiting for Apa yang baru button to load \033[1;37m")
except TimeoutException:
    print("\033[1;32m 006-Timed out waiting for Apa yang baru button to load \033[1;37m")
    #browser.close()
    
try:
    e = browser.find_element(By.CLASS_NAME,"css-evb2vf-unf-btn")
    e.click()
    print("\033[1;32m 006-Click Success \033[1;37m")
except:
    print("\033[1;32m 006-Click Failed \033[1;37m")

try:
    element_present = EC.presence_of_element_located((By.XPATH,"/html/body/div[6]/div/section/div[2]/div/div[2]/div[1]"))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m 007-Success waiting for Lewati button to load \033[1;37m")
except TimeoutException:
    print("\033[1;32m 007-Timed out waiting for Lewati button to load \033[1;37m")
    #browser.close()
    
try:
    e = browser.find_element(By.XPATH,"/html/body/div[6]/div/section/div[2]/div/div[2]/div[1]")
    e.click()
    print("\033[1;32m 007-Click Success \033[1;37m")
except:
    print("\033[1;32m 007-Click Failed \033[1;37m")

try:
    element_present = EC.presence_of_element_located((By.XPATH,"/html/body/div[1]/div/div[2]/div/div/div[2]/div[3]/button[1]"))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m 008-Success waiting for Nanti saja button to load \033[1;37m")
except TimeoutException:
    print("\033[1;32m 008-Timed out waiting for Nanti saja button to load \033[1;37m")
    #browser.close()
    
try:
    e = browser.find_element(By.XPATH,"/html/body/div[1]/div/div[2]/div/div/div[2]/div[3]/button[1]")
    e.click()
    print("\033[1;32m 008-Click Success \033[1;37m")
except:
    print("\033[1;32m 008-Click Failed \033[1;37m")

time.sleep(50)

try:
    element_present = EC.presence_of_element_located((By.PARTIAL_LINK_TEXT,"Wawasan Produk"))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m Success waiting for wawasan product link \033[1;37m")
except TimeoutException:
    print("\033[1;32m Timed out waiting for wawasan product link \033[1;37m")
    #browser.close()
e = browser.find_element(By.PARTIAL_LINK_TEXT,"Wawasan Produk")
e.click()

try:
    element_present = EC.presence_of_element_located((By.XPATH,"/html/body/div[5]/div[7]/section/div/div/div[2]/div"))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m Success waiting for pilihan kalender \033[1;37m")
except TimeoutException:
    print("\033[1;32m Timed out waiting for pilihan kalender \033[1;37m")
    #browser.close()
try:
    e = browser.find_element(By.XPATH,"/html/body/div[5]/div[7]/section/div/div/div[2]/div")
    e.click()
except:
    print("\033[1;32m Failed kalender \033[1;37m")

try:
    element_present = EC.presence_of_element_located((By.XPATH,"/html/body/div[6]/div[7]/section/div/div/div[2]/div[1]"))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m Success waiting for Lewati \033[1;37m")
except TimeoutException:
    print("\033[1;32m Timed out waiting for Lewati \033[1;37m")
    #browser.close()
try:    
    e = browser.find_element(By.XPATH,"/html/body/div[6]/div[7]/section/div/div/div[2]/div[1]")
    e.click()
except:
    print("\033[1;32m Failed Lewati \033[1;37m")

time.sleep(10)

browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")

kondisi=False
timeout=20

connection = psycopg2.connect(user="moshub_konso",
                                  password="konsomoshub",
                                  host="13.251.7.50",
                                  port="39726",
                                  database="dbskonso")
cursor = connection.cursor()

postgres_delete_query = """
                delete from t_wawasanprod where username=%s and platform=%s and partner=%s
                """
where_clause = (_username,_platform,_partner)
cursor.execute(postgres_delete_query, where_clause)
connection.commit()

while not kondisi:
    try:
        element_present = EC.presence_of_element_located((By.CSS_SELECTOR,'[aria-label="Halaman berikutnya"]'))
        WebDriverWait(browser, timeout).until(element_present)
        print("\033[1;32m Success waiting for next page \033[1;37m")
    except TimeoutException:
        print("\033[1;32m Timed out waiting for next page \033[1;37m")
        #browser.close()
    e = browser.find_element(By.CSS_SELECTOR,'[aria-label="Halaman berikutnya"]')
    prop = e.get_property('disabled')
    kondisi=prop
    #print("")
    p = browser.find_element(By.CLASS_NAME,"css-1ptv35")
    f = p.get_attribute('innerHTML')
    #print(f)        
    soup = BeautifulSoup(f,features="html.parser")
    table = soup.find("table")
    output_rows = []
    for table_row in table.findAll('tr'):
        columns = table_row.findAll('td')
        output_row = []
        for column in columns:
            output_row.append(column.text)
            #print("test ",column.text)
        #output_rows.append(output_row)
        insdata = output_row
        #print(insdata)
        if (insdata[1]!=""):
            _text=insdata[1]
            _cari = 'SKU:'
            _cari2 = ' - Kategori:'
            z = _text.index(_cari)
            z2 = _text.index(_cari2)
            _nmproduk = _text[0:(z)]
            _sku = _text[(z+5):(z2)]
            _kategori = _text[(z2+13):(len(_text))]
            #print('nmproduk   : ',_nmproduk)
            #print('sku        : ',_sku)
            #print('kategori   : ',_kategori)
            now = datetime.now()
            dt_string = now.strftime("%m/%d/%Y %H:%M:%S")
            #print("tglproses  : ", dt_string)
            _text=insdata[2]
            _pendapatan=_text.replace("Rp","",1)
            _pendapatan=_pendapatan.replace(".","",10)
            #print('pendapatan : ',_pendapatan)
            _terjual=insdata[3].replace(".","",10)
            #print('terjual    : ',_terjual)
            _dilihat=insdata[4].replace(".","",10)
            #print('dilihat    : ',_dilihat)
            _konversi=insdata[7].replace("%","",1)
            _konversi=_konversi.replace(",",".",1)
            #print('konversi    : ',_konversi)
            postgres_insert_query = """
                insert into t_wawasanprod (
                    username,platform,partner,prosesdate,sku,
                    nmproduk,pendapatan,terjual,dilihat,konversi,kategori
                )
                values 
                (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"""
            record_to_insert = (_username,_platform,_partner,dt_string,_sku,
                _nmproduk,_pendapatan,_terjual,_dilihat,_konversi,_kategori)
            cursor.execute(postgres_insert_query, record_to_insert)

    connection.commit()
    #print(output_rows)
    if not kondisi:
        e.click()

cursor.close()
connection.close()

time.sleep(20)
print("===== Success =========")
#browser.close()
