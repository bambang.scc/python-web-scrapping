#pip install -U selenium
#pip install beautifulsoup4
#https://codebeautify.org/htmlviewer
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.action_chains import ActionChains
from bs4 import BeautifulSoup

print("\033[1;32m Start Scrapping Tokopedia \033[1;37m")
browser = webdriver.Chrome("C:\Belajar\Python-Web-Scrapping\chromedriver.exe")
browser.get('https://www.tokopedia.com/login?ld=https://seller.tokopedia.com/')
e = browser.find_element(By.ID,"email-phone")
e.send_keys("bambang.scc@gmail.com")
e = browser.find_element(By.ID,"email-phone-submit")
e.click()
timeout = 10
try:
    element_present = EC.presence_of_element_located((By.ID, 'password-input'))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m Success waiting for password page to load \033[1;37m")
except TimeoutException:
    print("\033[1;32m Timed out waiting for password page to load \033[1;37m")
    e.close()

e = browser.find_element(By.ID,"password-input")
e.send_keys("BayuPassword71")
e = browser.find_element(By.CLASS_NAME,"css-ow4jg3-unf-btn")
e.click()

timeout = 1000    
try:
    element_present = EC.presence_of_element_located((By.CLASS_NAME, 'css-evb2vf-unf-btn'))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m Success waiting for main page to load \033[1;37m")
except TimeoutException:
    print("\033[1;32m Timed out waiting for main page to load \033[1;37m")
    e.close()

timeout = 10
try:
    element_present = EC.presence_of_element_located((By.CLASS_NAME, 'css-evb2vf-unf-btn'))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m Success waiting for click-1 \033[1;37m")
except TimeoutException:
    print("\033[1;32m Timed out waiting for click-1 \033[1;37m")
    e.close()
e = browser.find_element(By.CLASS_NAME,"css-evb2vf-unf-btn")
e.click()

try:
    element_present = EC.presence_of_element_located((By.CLASS_NAME, 'unf-coachmark__skip-button'))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m Success waiting for click-2 \033[1;37m")
except TimeoutException:
    print("\033[1;32m Timed out waiting for click-2 \033[1;37m")
    e.close()
e = browser.find_element(By.CLASS_NAME,"unf-coachmark__skip-button")
e.click()

try:
    element_present = EC.presence_of_element_located((By.CLASS_NAME, 'css-1qrjwth-unf-btn'))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m Success waiting for click-3 \033[1;37m")
except TimeoutException:
    print("\033[1;32m Timed out waiting for click-3 \033[1;37m")
    e.close()
e = browser.find_element(By.CLASS_NAME,"css-1qrjwth-unf-btn")
e.click()

try:
    element_present = EC.presence_of_element_located((By.PARTIAL_LINK_TEXT,"Wawasan Produk"))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m Success waiting for wawasan product link \033[1;37m")
except TimeoutException:
    print("\033[1;32m Timed out waiting for wawasan product link \033[1;37m")
    e.close()
e = browser.find_element(By.PARTIAL_LINK_TEXT,"Wawasan Produk")
e.click()

try:
    e = WebDriverWait(browser, timeout).until(EC.presence_of_element_located((By.CLASS_NAME, 
        'css-9hj4h0')))
    action = ActionChains(browser)
    action.move_to_element(e).move_by_offset(250, 0).click().perform()
    print("\033[1;32m Success waiting for click block modal \033[1;37m")
except TimeoutException:
    print("\033[1;32m Timed out waiting for click block modal \033[1;37m")
    e.close()

try:
    element_present = EC.presence_of_element_located((By.CLASS_NAME,"unf-coachmark__skip-button"))
    WebDriverWait(browser, timeout).until(element_present)
    print("\033[1;32m Success waiting for click-4 \033[1;37m")
except TimeoutException:
    print("\033[1;32m Timed out waiting for click-4 \033[1;37m")
    e.close()
e = browser.find_element(By.CLASS_NAME,"unf-coachmark__skip-button")
e.click()

browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")

kondisi=False
while not kondisi:
    try:
        element_present = EC.presence_of_element_located((By.CSS_SELECTOR,'[aria-label="Halaman berikutnya"]'))
        WebDriverWait(browser, timeout).until(element_present)
        print("\033[1;32m Success waiting for next page \033[1;37m")
    except TimeoutException:
        print("\033[1;32m Timed out waiting for next page \033[1;37m")
        e.close()
    e = browser.find_element(By.CSS_SELECTOR,'[aria-label="Halaman berikutnya"]')
    prop = e.get_property('disabled')
    kondisi=prop
    print("")
    p = browser.find_element(By.CLASS_NAME,"css-1ptv35")
    f = p.get_attribute('innerHTML')
    #print(f)        
    soup = BeautifulSoup(f,features="html.parser")
    table = soup.find("table")
    output_rows = []
    for table_row in table.findAll('tr'):
        columns = table_row.findAll('td')
        output_row = []
        for column in columns:
            output_row.append(column.text)
        output_rows.append(output_row)
    print(output_rows)
    if not kondisi:
        e.click()

#browser.close()

